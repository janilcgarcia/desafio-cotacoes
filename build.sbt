val Http4sVersion = "1.0.0-M33"
val LogbackVersion = "1.4.5"

lazy val root = (project in file("."))
  .settings(
    organization := "br.dev.safelydysfunctional",
    name := "cotacoes",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "3.2.1",
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-ember-server" % Http4sVersion,
      "org.http4s" %% "http4s-ember-client" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "ch.qos.logback" % "logback-classic" % LogbackVersion
    )
  )
