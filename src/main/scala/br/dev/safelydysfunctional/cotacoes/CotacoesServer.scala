package br.dev.safelydysfunctional.cotacoes

import cats.effect.syntax.resource.effectResourceOps
import cats.effect.IO

import org.http4s.ember.client.EmberClientBuilder
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.middleware.Logger

import com.comcast.ip4s.ipv4
import com.comcast.ip4s.port

object CotacoesServer {
  /** Runs http4s server
    */
  def run: IO[Nothing] = {
    for {
      // create dependecies
      client <- EmberClientBuilder.default[IO].build
      baseApp <- routes(client).toResource
      httpApp = Logger.httpApp(true, true)(baseApp.orNotFound)

      // create server
      _ <-
        EmberServerBuilder
          .default[IO]
          .withHost(ipv4"0.0.0.0")
          .withPort(port"9000")
          .withHttpApp(httpApp)
          .build
    } yield ()
  }.useForever
}
