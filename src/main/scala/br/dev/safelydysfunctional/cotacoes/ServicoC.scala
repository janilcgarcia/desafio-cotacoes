package br.dev.safelydysfunctional.cotacoes

import cats.syntax.flatMap._
import cats.syntax.functor._

import cats.effect.std.Queue
import cats.effect.syntax.monadCancel._
import cats.effect.Concurrent
import cats.effect.IO
import cats.effect.Ref

import fs2.concurrent.Topic

import org.http4s.circe.CirceEntityCodec._
import org.http4s.client.dsl.io._
import org.http4s.client.Client
import org.http4s.Method.POST
import org.http4s.Uri

import io.circe.Codec

/** Base class for ServicoC, since it has different implementations
  *
  * NOTE: any mention of blocking in this class means semantic blocking
  *
  * @param client A http4s HTTP Client
  * @param base The base URL for the service host
  * @param selfUri The callback url, where the service should return us with the exchange rate
  */
abstract class ServicoC(
    client: Client[IO],
    base: Uri,
    selfUri: Uri
) {
  import ServicoC._

  protected def doRequest(currency: String): IO[StartResponse] =
    client.expect(
      POST(
        StartRequest(currency, selfUri.toString),
        base / "servico-c" / "cotacao"
      )
    )

  /** Get the exchange rate for the currency.
    * 
    * If the service fails to return the exchange it will block until cancelled. It has no built-in timeout.
    *
    * @param currency Currency
    * @return The exchange rate
    */
  def getRate(currency: String): IO[BigDecimal]

  /** Action to solve getRate, should be called when the callback is received
    *
    * May block until the callback is processed.
    */
  def callback(callback: CotacaoCallback): IO[Unit]
}

object ServicoC {
  /** Payload of the request made to ServicoC to get the exchange rate
    * 
    * @param tipo currency to get the exchange rate for 
    * @param callback the URL where ServicoC should return the exchange rate
    */
  case class StartRequest(
      tipo: String,
      callback: String
  ) derives Codec.AsObject

  /** Response payload for the initial request, but the callback, that's covered on CotacaoCallback.
    *
    * @param mood Status of the response
    * @param cid Token used to identify the callback
    * @param messagem Message reguarding the status
    */
  case class StartResponse(
      mood: String,
      cid: String,
      mensagem: String
  ) derives Codec.AsObject

  /** Builder for ServicoC
    * 
    * @param client http4s HTTP Client
    * @param serviceBase the base URI
    * @param callbackUrl the url the service should return to when the exchange rate is ready
    */
  case class Builder(client: Client[IO], serviceBase: Uri, callbackUrl: Uri) {
    /** Creates a ServioC using a queue-based implementation
      *
      * This implementation works by creating synchronous queues (needs both a offerer and taker) for which open
      * request. The synchronous queue require that both ends of the queue have active fibers to allow communication,
      * in practice allowing getRate to finish only when callback is called and vice-versa. That can lead to dead-lock
      * to avoid that callback should be run a background fiber when handling the request.
      */
    def queue: IO[ServicoC] =
      IO.ref(Map.empty).map(QueueImpl(_, client, serviceBase, callbackUrl))

    /** Creates a ServicoC using a fs2 Stream-based implementation
      *
      * This implementation works by creating a Topic (pub-sub archicture). getRate subscribes to callbacks and finds
      * the first that matches it's id and callback publishes callbacks to the Topic.
      * 
      * Although conceptually very simple, may be slower.
      */
    def stream: IO[ServicoC] =
      Topic[IO, CotacaoCallback].map(StreamImpl(_, client, serviceBase, callbackUrl))
  }

  /** Queue-based implemenation for ServicoC
    *
    * @param ref This ref holds a map from a cid (callback token) to synchronous queues
    */
  private class QueueImpl(
      ref: Ref[IO, Map[String, Queue[IO, BigDecimal]]],
      client: Client[IO],
      base: Uri,
      selfUri: Uri
  ) extends ServicoC(client, base, selfUri) {
    import ServicoC.*

    /** Add a new queue to ref, if one does not exist.
      *
      * @param id ID for mapping the queue
      */
    private def setup(id: String) = Queue.synchronous[IO, BigDecimal].flatMap { localQueue =>
      ref.modify { waiting =>
        waiting.get(id) match {
          case Some(q) => (waiting, q)
          case None    => (waiting + (id -> localQueue), localQueue)
        }
      }
    }

    /** Remove queue from ref to cleanup after queue usage
      */
    private def cleanup(id: String) = ref.update(_ - id)

    def getRate(currency: String): IO[BigDecimal] =
      doRequest(currency).flatMap { response =>
        // Uses bracket to ensure that if a queue is created and inserted it is eventually removed
        // to avoid a memory leak
        setup(response.cid).bracket(_.take)(_ => cleanup(response.cid))
      }

    def callback(callback: CotacaoCallback): IO[Unit] =
      // Check bracket reasons in getRate
      setup(callback.cid).bracket(_.offer(callback.rate))(_ => cleanup(callback.cid))
  }

  /** Stream-based implementation of ServicoC
    *
    * @param topic The topic where callbacks are published to.
    */
  private class StreamImpl(
      topic: Topic[IO, CotacaoCallback],
      client: Client[IO],
      base: Uri,
      self: Uri
  ) extends ServicoC(client, base, self) {
    def getRate(currency: String): IO[BigDecimal] =
      // uses subscribe await to start the subscription before starting the request to avoid issues in case
      // the callback is called before doRequest finishes
      topic.subscribeAwait(10).use { stream =>
        for {
          response <- doRequest(currency)

          response <- stream
            .collectFirst {
              case c if c.cid == response.cid => c.rate
            }
            .compile
            .lastOrError
        } yield response
      }

    def callback(callback: CotacaoCallback): IO[Unit] =
      // This actually shouldn't block in normal circustances
      topic.publish1(callback).void
  }
}
