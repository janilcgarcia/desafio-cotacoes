package br.dev.safelydysfunctional.cotacoes

import cats.effect.IO
import cats.effect.IOApp

object Main extends IOApp.Simple {
  val run = CotacoesServer.run
}
