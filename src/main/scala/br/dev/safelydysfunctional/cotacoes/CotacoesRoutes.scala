package br.dev.safelydysfunctional.cotacoes

import scala.concurrent.duration.DurationInt

import cats.syntax.all._

import cats.effect.IO
import cats.effect.Outcome

import org.http4s.circe.CirceEntityCodec.*
import org.http4s.client.Client
import org.http4s.dsl.io._
import org.http4s.syntax.literals.uri
import org.http4s.HttpRoutes

import io.circe.Codec

/** Response payload object for our API
  * @param cotacao Least value for the rate obtained from all services
  * @param moeda Currency being queried for an exchange rate
  * @param comparativo Currency being used as the basis for the exchange rate. Always BRL in this application.
  */
case class Cotacoes(
    cotacao: BigDecimal,
    moeda: String,
    comparativo: String
) derives Codec.AsObject

/** Create HTTP routes
  * @param an HTTP client for accessing the external services
  * @returns HTTPRoutes wrapped in IO. ServiceC needs to be created in IO.
  */
def routes(client: Client[IO]): IO[HttpRoutes[IO]] = {
  // Should be configurable
  val serviceBase = uri"http://localhost:8080/"
  val callbackUrl = uri"http://host.containers.internal:9000/servico-c-callback"

  val builder = ServicoC.Builder(client, serviceBase, callbackUrl)

  // Creates ServicoC using both Ref + Map + Queue and fs2 Topic/Stream
  builder.queue.both(builder.stream).map { (queueC, streamC) =>
    val getRates = Seq(
      ServicoA.getRate(client, serviceBase),
      ServicoB.getRate(client, serviceBase),
      queueC.getRate,
      streamC.getRate
    )

    HttpRoutes.of[IO] {
      case GET -> Root / "cotacoes" / currency =>
        // Get the rate from all services in parallel
        val results =
          getRates
            .map(getRate => getRate(currency).timeout(5.second).attempt)
            .parSequence

        for {
          // Under the possibility of no getRate returning a valid value, use minimumOption
          cotacao <- results.map(_.collect { case Right(result) =>
            result
          }.minimumOption)

          r <- cotacao match {
            case Some(cotacao) =>
              Ok(
                Cotacoes(
                  cotacao = cotacao,
                  moeda = currency,
                  comparativo = "BRL"
                )
              )

            case None =>
              ServiceUnavailable(
                Map(
                  "errors" -> "The server wasn't able to fetch the exchange rate"
                )
              )
          }
        } yield r

      case req @ POST -> Root / "servico-c-callback" =>
        req.as[CotacaoCallback].flatMap { callback =>
          // As we don't know which service start this callback, we just send to both. If resource management works
          // well this is never a problem: unused callbacks will just drop.
          queueC
            .callback(callback)
            .both(streamC.callback(callback))
            .timeout(1.second)
            .start >>
            NoContent()
        }
    }
  }
}
