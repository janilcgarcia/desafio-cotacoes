package br.dev.safelydysfunctional.cotacoes

import cats.syntax.functor._

import cats.effect.IO

import org.http4s.circe.CirceEntityCodec._
import org.http4s.client.Client
import org.http4s.syntax.literals.uri
import org.http4s.Uri

import io.circe.Codec

object ServicoA {
  /** Response from ServicoA
    *
    * @param cotacao Exchange rate
    * @param moeda Currency
    * @param symbol The currency symbol
    */
  case class Cotacao(
      val cotacao: BigDecimal,
      val moeda: String,
      val symbol: String
  ) derives Codec.AsObject

  /** Get rate in ServicoA.
    *
    * Parameters are split so getRate(..., ...) may produce a partial function that can be called with other functions
    * with the same interface
    * 
    * @param client a http4s HTTP client
    * @param base the URI base (http://.../) for the external service
    * @param currency currency to lookup the exchange rate
    * @return The exchange rate for the currency, if everything works
    */
  def getRate(client: Client[IO], base: Uri)(currency: String): IO[BigDecimal] = {
    val url = base / "servico-a" / "cotacao" +? ("moeda" -> currency)

    client.expect[Cotacao](url).map(_.cotacao)
  }
}
