package br.dev.safelydysfunctional.cotacoes

import io.circe.Codec

/** Request payload from the callback on ServicoC
  *
  * @param cid the request ID
  * @param f factor by which v was multiplied to avoid FP errors 
  * @param t currency
  * @param v value multiplied by f
  */
case class CotacaoCallback(
    cid: String,
    f: Int,
    t: String,
    v: Int
) derives Codec.AsObject {
  /** Get the exchange rate in this payload
    */
  lazy val rate: BigDecimal = BigDecimal(v) / f
}
