package br.dev.safelydysfunctional.cotacoes

import cats.syntax.functor._

import cats.effect.IO

import org.http4s.circe.CirceEntityCodec._
import org.http4s.client.Client
import org.http4s.Uri

import io.circe.Decoder
import io.circe.Encoder
import io.circe.Json

object ServicoB {
  /** Response payload from ServicoB.
    *
    * @param fator fator to reduce the valor by. 3.456 would be represented as fator = 1000, valor = 3456
    * @param currency currency for the exchange rate
    * @param valor exchange rate, multiplied by fator and returned as a string to avoid FP errors
    */
  case class Cotacao(fator: Int, currency: String, valor: String) {
    /** Exchange rate for response. It may fail with an exception, but if the server respects it's own interface
      * it won't.
      */
    lazy val rate: BigDecimal = BigDecimal(valor) / fator
  }

  object Cotacao {
    /** Cotacao can't use a simple Codec.AsObject because the value is nested into an object.
      */
    given circeDecoder: Decoder[Cotacao] =
      Decoder.derived[Cotacao].at("cotacao")
  }

    /** Get rate in ServicoB.
    *
    * Parameters are split so getRate(..., ...) may produce a partial function that can be called with other functions
    * with the same interface
    * 
    * @param client a http4s HTTP client
    * @param base the URI base (http://.../) for the external service
    * @param currency currency to lookup the exchange rate
    * @return The exchange rate for the currency, if everything works
    */
  def getRate(client: Client[IO], base: Uri)(currency: String): IO[BigDecimal] = {
    val url = base / "servico-b" / "cotacao" +? ("curr" -> currency)

    client.expect[Cotacao](url).map(_.rate)
  }
}
